package com.kryviak;

import com.kryviak.sentences.comparators.CompareByWordsCount;
import com.kryviak.sentences.comparators.CompareByWordsMatched;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class File {
    public static final String FILENAME = "textFile.txt";

    public static void main(String[] args) {
        String fullText = readTextFromFile();
        String searchWord = getSearchCriteria();
        List<SenteneObj> result = getSentencesList(fullText, searchWord);
        findWordInFirstSentences(result);
        moveFirstWord(result);
        Collections.sort(result, new CompareByWordsCount());
        System.out.println(result);
        Collections.sort(result, new CompareByWordsMatched());

        findQuestionInText(fullText);
//        System.out.println(result);
    }

    private static List<SenteneObj> getSentencesList(String fullText, String searchWord) {
        List<SenteneObj> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("[^.?]*[.?]");
        Matcher m = pattern.matcher(fullText);

        while (m.find()) {
            list.add(new SenteneObj(getWordsCount(fullText.substring(m.start(), m.end())),
                    getWordsMatchedCount(fullText.substring(m.start(), m.end()), searchWord),
                    fullText.substring(m.start(), m.end())));
        }
        return list;
    }

    private static String getSearchCriteria() {
        System.out.println("Please enter the word that you want to find: ");
        Scanner scan = new Scanner(System.in);
        return scan.next();
    }

    private static int getWordsCount(String sentence) {
        // пробіл один або декілька разів не включаючи .!? один або декілька разів і закінчується пробілом або .!?
        Pattern pattern = Pattern.compile("\\s*[^.!?\\s]*[\\s.!?]");
        Matcher m = pattern.matcher(sentence);
        int count = 0;
        while (m.find()) {
            sentence.substring(m.start(), m.end());
            count++;
        }
        return count;
    }

    private static int getWordsMatchedCount(String sentence, String searchWord) {
        int count = 0;
        int index = sentence.indexOf(searchWord);
        while (index != -1) {
            count++;
            sentence = sentence.substring(index + 1);
            index = sentence.indexOf(searchWord);
        }
        return count;
    }

    private static String readTextFromFile() {
        StringBuilder str = new StringBuilder();
        try (FileInputStream fstream = new FileInputStream(FILENAME)) {
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;

            while ((strLine = br.readLine()) != null) {
                str.append(strLine + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return str.toString();
    }

    private static void findWordInFirstSentences(List<SenteneObj> sentencesObj) {
        String firstSentences = sentencesObj.get(0).getSentence();
        Pattern pattern = Pattern.compile("\\s*[^.!?\\s]*[\\s.!?]");
        Matcher m = pattern.matcher(firstSentences);
        while (m.find()) {
            firstSentences.substring(m.start(), m.end());
            for (int i = 1; i < sentencesObj.size(); i++) {
                if (!sentencesObj.get(i).getSentence().contains(firstSentences.substring(m.start(), m.end()))) {
                    System.out.println("Word '" + firstSentences.substring(m.start(), m.end()) + "' isn't present in sentences");
                    return;
                }
            }
        }
    }

    private static void findQuestionInText(String fullText) {
        Pattern pattern = Pattern.compile("[^.]*[?]");
        Matcher m = pattern.matcher(fullText);

        while (m.find()) {
            System.out.println("Question sentences" + fullText.substring(m.start(), m.end()));
        }
    }

    private static void moveFirstWord(List<SenteneObj> sentencesObj) {
        Pattern pattern = Pattern.compile("[0-9]+[^\\s]*[.\\s]");
        for (int i = 0; i < sentencesObj.size(); i++) {
            String oneSentence = sentencesObj.get(i).getSentence();
            Matcher m = pattern.matcher(oneSentence);
            while (m.find()) {
                String firstWord = oneSentence.substring(m.start(), m.end());
                String theBiggestWord = gettheLongestWord(sentencesObj.get(i).getSentence());
                String newSentences = oneSentence.replace(firstWord, theBiggestWord);
                System.out.println("Move the biggest sentences " + newSentences);
                break;
            }
        }
    }

    private static String gettheLongestWord(String sentence) {
        String[] word = sentence.split(" ");
        String maxlethWord = "";
        for (int i = 0; i < word.length; i++) {
            if (word[i].length() >= maxlethWord.length()) {
                maxlethWord = word[i];
            }
        }
        return maxlethWord;
    }


}

