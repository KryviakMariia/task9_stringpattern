package com.kryviak;

public class SenteneObj {
    private int wordsCount;
    private int wordsMatchedCount;
    private String sentence;

    public SenteneObj(int wordsCount, int wordsMatchedCount, String sentence) {
        this.wordsCount = wordsCount;
        this.wordsMatchedCount = wordsMatchedCount;
        this.sentence = sentence;
    }

    public int getWordsCount() {
        return wordsCount;
    }

    public void setWordsCount(int wordsCount) {
        this.wordsCount = wordsCount;
    }

    public int getWordsMatchedCount() {
        return wordsMatchedCount;
    }

    public void setWordsMatchedCount(int wordsMatchedCount) {
        this.wordsMatchedCount = wordsMatchedCount;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public String toString() {
        return "SentenceObj{ wordsCount = " + wordsCount + ", wordsMatchedCount = " + wordsMatchedCount + ", sentence = '" + sentence + "' }\n";
    }
}
