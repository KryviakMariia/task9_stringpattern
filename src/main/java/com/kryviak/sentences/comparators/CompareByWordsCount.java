package com.kryviak.sentences.comparators;

import com.kryviak.SenteneObj;

import java.util.Comparator;

public class CompareByWordsCount implements Comparator<SenteneObj> {

    public int compare(SenteneObj o1, SenteneObj o2) {
        return Integer.compare(o1.getWordsCount(), o2.getWordsCount());
    }
}
