package com.kryviak.localization;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

public class Menu {
    public static final String PATH_TO_ENGLISH_FILE = "src/main/resources/localization/en.properties";
    public static final String PATH_TO_UKRAINIAN_FILE = "src/main/resources/localization/uk.properties";
    public static final String PATH_TO_JAPANESE_FILE = "src/main/resources/localization/ja.properties";
    public static final int MENU_ITEMS = 4;

    public static void main(String[] args) {
        chooseLanguage();
    }

    private static void printMenu(String pathTofile) {
        Properties property = new Properties();

        try (FileInputStream fis = new FileInputStream(pathTofile)){
            property.load(fis);

            for (int i = 0; i < MENU_ITEMS; i++) {
                String str1 = Integer.toString(i);
                System.out.println(i +" " + property.getProperty(str1));
            }

        } catch (IOException e) {
            System.err.println("Exception. File not found");
        }
    }

    private static void chooseLanguage() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number of language: ");

        int input;
        do {
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    printMenu(PATH_TO_ENGLISH_FILE);
                    break;
                case 2:
                    printMenu(PATH_TO_UKRAINIAN_FILE);
                    break;
                case 3:
                    printMenu(PATH_TO_JAPANESE_FILE);
                    break;
            }
        } while (input != 0);
    }
}
